package com.example.accountabilitybuddyapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginActivity  extends AppCompatActivity {

    private FirebaseAuth mAuth;
    Button btnLogin;
    TextView signupLink;
    EditText etEmail;
    EditText etPassword;
    TextView tvForgotPassword;

    FirebaseDatabase database;
    DatabaseReference myRef;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            mAuth = FirebaseAuth.getInstance();
            btnLogin = findViewById(R.id.btn_show);
            etEmail = findViewById(R.id.et_useremail_info);
            etPassword = findViewById(R.id.et_password_info);
            database  = FirebaseDatabase.getInstance();
            myRef = database.getReference("User");
            prefs = this.getSharedPreferences(
                    "com.example.accountabilitybuddyapp", Context.MODE_PRIVATE);
            editor = prefs.edit();
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(validateData() == true){
                        validateuser(etEmail.getText().toString(), etPassword.getText().toString());
                    }
                }
            });

            signupLink = findViewById(R.id.tv_signup);
            signupLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mAuth.signOut();
                    Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                    startActivity(intent);

                }
            });
            tvForgotPassword =findViewById(R.id.tv_forgot_pass);
            tvForgotPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, ForgotPassActivity.class);
                    startActivity(intent);

                }
            });
        }

    private boolean validateData() {
         if (etEmail.getText().toString().equals("")){
            Toast.makeText(LoginActivity.this, "Please enter Email", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (isEmailValid(etEmail.getText().toString()) == false){
            Toast.makeText(LoginActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
            return false;
        }

        else if (etPassword.getText().toString().equals("")){
            Toast.makeText(LoginActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }

        else {
            return true;
        }
    }

    private boolean checkString(String str) {
        char ch;
        boolean capitalFlag = false;
        boolean lowerCaseFlag = false;
        boolean numberFlag = false;
        for(int i=0;i < str.length();i++) {
            ch = str.charAt(i);
            if( Character.isDigit(ch)) {
                numberFlag = true;
            }
            else if (Character.isUpperCase(ch)) {
                capitalFlag = true;
            } else if (Character.isLowerCase(ch)) {
                lowerCaseFlag = true;
            }
            if(numberFlag && capitalFlag && lowerCaseFlag)
                return true;
        }
        return false;
    }

    private boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public void validateuser(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            getUserFromDB(user.getUid());
                        } else {
                            // If sign in fails, display a message to the user.

                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }

    private void getUserFromDB(final String uid) {

           //access to notepad and folder called User
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("User");
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                for(DataSnapshot childSnapShot: dataSnapshot.getChildren()) {
                    User value = childSnapShot.getValue(User.class);
                    if (value.getUid().equals(uid)) { editor.putString("name", value.getName());
                    editor.putString("email", value.getEmail());
                    editor.putString("Uid", value.getUid());
                    editor.commit();
                    Toast.makeText(LoginActivity.this, "Successfully Authenticated", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                        startActivity(intent);
                        finish();

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

            }
        });

    }
}


